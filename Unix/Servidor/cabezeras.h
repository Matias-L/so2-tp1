/** 
*@brief Archivo de cabezera que se debe incluir en todos los codigos fuentes
*
*   
*@author Matias Lionel Ceballos
*@date Abril 2019
*/
#ifndef CABEZERAS_H_
#define CABEZERAS_H_
#define TAM 80
/** Librerias usados por los distintos codigos fuente
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>

/** Funciones que escribí
*/

int login(int);
void promptInvitado(int);
void promptUsuario(int);
int sesionValidada(int);
int updateFirmware(int);
int startScanning(int);
int obtenerTelemetria(int);




#endif /*CABEZERAS_H_*/
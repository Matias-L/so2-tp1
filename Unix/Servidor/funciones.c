#include "cabezeras.h"
#define TAM 80

/**
	@file funciones.c
	@author Ceballos, Matias Lionel
	@date Abril 2019

*/


/**
*@brief Funcion para el log in del cliente.
*
*Primero envia el prompt de invitado al cliente. Otorga 3 intentos al cliente para validar su usuario.
*Queda a la espera de los mensajes del cliente, al recibirlo los "tokeniza", y los compara con 
*el usuario que esta registrado (a futuro planeo agregar mas usuarios).
*El cliente debe enviar el usuario y contraseña con el formato:
*@code
*<usuario>:<contraseña>
*
*@param int Socket file descriptor
*@return 1 Si el usuario es valido, 0 en caso de que se agoten los intentos
*/
int login(int socket){

	char buffer[TAM];
	char* usuario="";
	char* contrasenia="";
	int n=0;
	int intentos=0;

	while(intentos<3){	
	promptInvitado(socket);	
		memset( buffer, '\0', TAM );

		n = read( socket, buffer, TAM-1 );
			if ( n < 0 ) {
			perror( "lectura de socket" );
			exit(1);
			}

		usuario=strtok(buffer, ":");
		contrasenia=strtok(NULL, "\n");

		printf("Usuario: %s || Contraneña: %s \n", usuario, contrasenia);

			if((!strcmp(usuario, "matute"))&&(!strcmp(contrasenia, "444"))){
				printf("Un satelite se conecto exitosamente.\n");
				n = write( socket, "Bienvenido.", 12 );
					if ( n < 0 ) {
					perror( "escritura en socket" );
					exit( 1 );
					}

				return 1;
			}
			else{
				n = write( socket, "CRECENCIALES INVALIDAS", 23 );
					if ( n < 0 ) {
					perror( "escritura en socket" );
					exit( 1 );
					}
				intentos++;	
			}
	}//Fin while intentos

	printf("Un satelite NO ha logrado conectarse.\n");
	n = write( socket, "RECHAZADO", 10 );
		if ( n < 0 ) {
		perror( "escritura en socket" );
		exit( 1 );
		}
	return 0;

}

/**
	@brief Prompt que se envía al cliente para cuando no está validado por el sistema

	
	@param Socket file descriptor a donde se envia el prompt
*/


void promptInvitado(int socket){


	int n = write( socket, "Invitado> ", 11 );
		if ( n < 0 ) {
		perror( "escritura en socket" );
		exit( 1 );
	}

}

/**
	@brief Prompt que se envía al cliente para cuando está validado por el sistema

	
	@param Socket file descriptor a donde se envia el prompt
*/


void promptUsuario(int socket){


	int n = write( socket, "Satelite~> ", 11 );
		if ( n < 0 ) {
		perror( "escritura en socket" );
		exit( 1 );
	}
	printf("Enviado promptUsuario\n");
}


/**
	@brief Función que servirá los comandos al usuario validado

	Primero envia el prompt al usuario validado, y luego se coloca a la espera de comandos del cliente.
	Al recibir un comando, se compara y se sirve la función correspondiente.
	
	@param Socket file descriptor con el que se está conectado
	@return 0 Para finalizar el programa
*/

int sesionValidada(int socket){

	int n=0;
	char buffer[TAM];
	int sesionActiva=1;
	printf("Ejecutando sesion valida\n");

	n = read( socket, buffer, TAM-1 );
		if ( n < 0 ) {
		perror( "lectura de socket" );
		exit(1);
		}



	promptUsuario(socket);


	while(sesionActiva){
	//Espero que el usuario ingrese un comando
	memset( buffer, '\0', TAM );
	printf("Esperando ordenes...\n");
	n = read( socket, buffer, TAM-1 );
		if ( n < 0 ) {
		perror( "lectura de socket" );
		exit(1);
		}
				buffer[strlen(buffer)-1] = '\0';

		if(!strcmp(buffer, "updateFirmware")){
			n=updateFirmware(socket);
			sesionActiva=0;
		}

		if(!strcmp(buffer, "startScanning")){
			n=startScanning(socket);
		}

		if(!strcmp(buffer, "obtenerTelemetria")){
			n=obtenerTelemetria(socket);
		}
	}//Fin while sesion activa
return 0;
}

/**
	@brief updateFirmware

	a implementar
	
	@param a implementar
	@return a implementar
*/


int updateFirmware(int socket){
	int n = write( socket, "updateFirmware", 15 );
		if ( n < 0 ) {
		perror( "escritura en socket" );
		exit( 1 );
	}
	printf("updateFirmware\n");
	return 0;
}

/**
	@brief startScanning

	a implementar
	
	@param a implementar
	@return a implementar
*/

int startScanning(int socket){
	int n = write( socket, "startScanning", 14 );
		if ( n < 0 ) {
		perror( "escritura en socket" );
		exit( 1 );
	}
	printf("startScanning\n");
	return 0;
}


/**
	@brief obtenerTelemetria

	a implementar
	
	@param a implementar
	@return a implementar
*/

int obtenerTelemetria(int socket){
	int n = write( socket, "obtenerTelemetria", 18 );
		if ( n < 0 ) {
		perror( "escritura en socket" );
		exit( 1 );
	}
	printf("obtenerTelemetria\n");
	return 0;
}


#include "cabezeras.h"
#define TAM 80

/**	@file main.c
	@brief Funcion principal del servidor (estación terrena).

	Comienza creando un socket Unix orientado a la conexión. El usuario debe colocar por linea de 
	comandos el socket a utilizar. Una vez realizada la conexión con un cliente, se atiende 
	dicha conexión mediante un proceso hijo. Este proceso se ejecutará mientras la variable "sesiónActiva"
	sea distinta de cero. Caso contrario, se cierra el socket y se elimina el proceso.
	El cliente que desea conectarse debe ingresar un usuario y contraseña. Luego de 3 intentos
	fallidos, se le rechaza la conexión.
	Si la conexión es aceptada, se invoca a la función "sesionValidada":
	@code 
	int sesionValidada(int socket_filedescriptor)
	, la cual esperará los
	comandos ingresados por el usuario, y las servirá al mismo.
   
	@author Ceballos, Matias Lionel
	@date Abril 2019
*/


int main(int argc, char *argv[]) {
	int sockfd, newsockfd, servlen, n, pid;
	int sesionActiva=1;
	int servidorVivo=1;
	socklen_t clilen;
	struct sockaddr_un  cli_addr, serv_addr;
	char buffer[TAM];

        /* Se toma el nombre del socket de la línea de comandos */
        if( argc != 2 ) {
                printf( "Uso: %s <nombre_de_socket>\n", argv[0] );
                exit( 1 );
        }

	if ( ( sockfd = socket( AF_UNIX, SOCK_STREAM, 0) ) < 0 ) {
		perror( "creación de  socket");
		exit(1);
	}

        /* Remover el nombre de archivo si existe */
        unlink ( argv[1] );

	memset( &serv_addr, 0, sizeof(serv_addr) );
	serv_addr.sun_family = AF_UNIX;
	strcpy( serv_addr.sun_path, argv[1] );
	servlen = strlen(serv_addr.sun_path) + sizeof(serv_addr.sun_family);

	if( bind( sockfd,(struct sockaddr *)&serv_addr,servlen )<0 ) {
		perror( "ligadura" ); 
		exit(1);
	}

        printf( "Proceso: %d - socket disponible: %s\n", getpid(), serv_addr.sun_path );

	listen( sockfd, 5 );
	clilen = sizeof( cli_addr );

        while ( servidorVivo ) {
                newsockfd = accept( sockfd, (struct sockaddr *) &cli_addr, &clilen );
                if ( newsockfd < 0 ) {
                        perror( "accept" );
                        exit( 1 );
                }

                pid = fork();
                if ( pid < 0 ) {
                        perror( "fork" );
                        exit( 1 );
                }

                if ( pid == 0 ) {        //proceso hijo
                        close( sockfd );

			while(sesionActiva) { 


				//Espero un primer mensaje
				memset( buffer, 0, TAM );

       		    n = read( newsockfd, buffer, TAM-1 );
	            	if ( n < 0 ) {
					perror( "lectura de socket" );
               		exit(1);
                    }

                printf( "PROCESO: %d. ", getpid() );
	            printf( "Recibí: %s", buffer );
				promptInvitado(newsockfd);
				if(login(newsockfd)){
					if(sesionValidada(newsockfd)){
						printf("Sesion viva?\n");
					}
					
				}
				else{
					printf("Cerrando conexion...\n");
					close(newsockfd);
					exit(0);
				}

		        //printf( "Recibí: %s", buffer );

				n = write( newsockfd, "Obtuve su mensaje", 18 );
					if ( n < 0 ) {
					perror( "escritura en socket" );
					exit( 1 );
					}
		        
			}//Fin while(1)
			//exit(0);
			//close(newsockfd);
                }
                else {
			printf( "SERVIDOR: Nuevo cliente, que atiende el proceso hijo: %d\n", pid );
                        //close( newsockfd );
		}
        }
    close(sockfd);
	return 0;
}

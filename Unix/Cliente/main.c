/**	@file main.c
	@brief Funcion principal del cliente ( satelite).

	Comienza creando un socket Unix orientado a la conexión. El usuario debe colocar por linea de 
	comandos el socket a utilizar (primero debe haber sido creado por el 
	servidor. Por defecto, está en la carpeta del servidor). 
	Una vez conectado al servidor, se ingresa a la función login:
	@code 
	int login(int socket_filedescriptor)

	La misma recibe el prompt de usuario invitado enviado por el servidor, y maneja el proceso
	de validación de credenciales.
	Si se realiza la validación, se ingresa a la función correspondiente a una sesión activa:
	@code
	int sesionActiva(int socket_filedescriptor)
   
	@author Ceballos, Matias Lionel
	@date Abril 2019
*/
#include "cabezeras.h"
#define TAM 80

int main( int argc, char *argv[] ) {
	int sockfd, servlen; 
	//int n;
	//int sesionViva=1;
	struct sockaddr_un serv_addr;
	//char buffer[TAM];
	//int terminar = 0;

        if ( argc < 2 ) {
                fprintf( stderr, "Uso %s archivo\n", argv[0]);
                exit( 0 );
        }

	memset( (char *)&serv_addr, '\0', sizeof(serv_addr) );
	serv_addr.sun_family = AF_UNIX;
	strcpy( serv_addr.sun_path, argv[1] );
	servlen = strlen( serv_addr.sun_path) + sizeof(serv_addr.sun_family);

	if ( (sockfd = socket(AF_UNIX, SOCK_STREAM, 0) ) < 0) {
		perror( "creación de socket" );
		exit( 1 );
	}


 	if ( connect( sockfd, (struct sockaddr *)&serv_addr, servlen ) < 0 ) {
		perror( "conexión" );
		exit( 1 );
	}

	//while(sesionViva) {

        if(login(sockfd)){
        printf("Logre conectarme.\n");
        sesionActiva(sockfd);
        }
        
        else{
        printf("Conexion no aceptada. Saliendo...\n");
        close(sockfd);
        exit(0);
        }
	//}
	return 0;
}

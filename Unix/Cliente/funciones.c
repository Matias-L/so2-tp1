#include "cabezeras.h"
#define TAM 80

/**
	@file funciones.c
	@author Ceballos, Matias Lionel
	@date Abril 2019

*/


/**
	@brief Funcion para el log in del cliente.

	Primero recibe el prompt de invitado enviado por el servidor, y se lo copia 
	a una variable para su uso.
	Cada vez que se envia una tupla usuario-contraseña, se espera
	la respuesta del servidor, y en función de eso, se procede con una
	sesión validada o se cierra la conexión en caso de no haber ingresado una tupla
	válida
	El cliente debe enviar el usuario y contraseña con el formato:
	@code
	<usuario>:<contraseña>

	@param int Socket file descriptor
	@return 1 Si el usuario es valido, 0 en caso de que se agoten los intentos
*/
int login(int socket){
	char buffer[TAM];
	int n=0;
	char prompt[TAM];
	int continuar=1;
	//Ejecuto esto solo una vez, para establecer conexion
	n=write( socket, "Pido prompt", 12);
    	if ( n < 0 ) {
        perror( "escritura de socket" );
        exit( 1 );
        }

	memset( buffer, '0', TAM );
	n = read( socket, buffer, TAM );
    	if ( n < 0 ) {
        perror( "lectura de socket" );
        exit( 1 );
        }
		buffer[strlen(buffer)-1] = '\0';
    strcpy(prompt, buffer);	//Copio el prompt enviado por el servidor
    						//Y lo muestro por pantalla

    while(continuar){
    	printf("%s", prompt);
		memset( buffer, '\0', TAM );
		fgets( buffer, TAM-1, stdin );
		n=write( socket, buffer, strlen(buffer) );
	    	if ( n < 0 ) {
	        perror( "escritura de socket" );
	        close(socket);
	        exit( 1 );
	        }

	    n = read( socket, buffer, TAM );
	    	if ( n < 0 ) {
	        perror( "lectura de socket" );
	        exit( 1 );
	        }
		//buffer[strlen(buffer)-1] = '\0';
		printf("%s\n", buffer );

		if(!strcmp(buffer, "Bienvenido.")){
			return 1;
		}
		if(!strcmp(buffer, "RECHAZADO")){
			return 0;
		}
	}
printf("No se deberia haber llegado aca\n");
return 0;
}


/**
	@brief Funcion que se ejecuta luego de haber ingresado usuario y contraseña válidos.

	Primero recibe el promt de usuario valido enviado por el servidor, y lo copia a una variable
	para su manipulacion mas facil.
	Luego, se entra a un bucle dependiente de la variable "sesionActiva". Mientras dicha variable sea
	distinta de cero, se pueden seguir ingresando comandos.

	@param int Socket file descriptor
*/

void sesionActiva(int socket){

	char buffer[TAM];
	int n=0;
	char prompt[TAM];
	int sesionActiva=1;
	//Ejecuto esto solo una vez, para establecer conexion
	printf("Ingresando a sesion valida\n");
	n=write( socket, "Pido prompt", 12);
    	if ( n < 0 ) {
        perror( "escritura de socket" );
        exit( 1 );
        }

	memset( buffer, '\0', TAM );
	n = read( socket, buffer, TAM );
    	if ( n < 0 ) {
        perror( "lectura de socket" );
        exit( 1 );
        }

		buffer[strlen(buffer)-1] = '\0';
//printf("%s\n",buffer );

    strcpy(prompt, buffer);

	while(sesionActiva){
	//Espero que el usuario ingrese un comando
	memset( buffer, '\0', TAM );
    	printf("%s", prompt);
		memset( buffer, '\0', TAM );
		fgets( buffer, TAM-1, stdin );
		n=write( socket, buffer, strlen(buffer) );
	    	if ( n < 0 ) {
	        perror( "escritura de socket" );
	        close(socket);
	        exit( 1 );
	        }
		buffer[strlen(buffer)-1] = '\0';
		//printf("Ingresaste: %s\n",buffer );
		if(!strcmp(buffer, "updateFirmware")){
			n=updateFirmware(socket);
			sesionActiva=0;
		}

		if(!strcmp(buffer, "startScanning")){
			n=startScanning(socket);
		}

		if(!strcmp(buffer, "obtenerTelemetria")){
			n=obtenerTelemetria(socket);
		}
	}//Fin while sesion activa

}

/**
	@brief updateFirmware

	a implementar

	@param a implementar
	@return a implementar
*/

int updateFirmware(int socket){
	printf("Ingresaste updateFirmware\n");	
	char buffer[TAM];
	int n = read( socket, buffer, TAM );
    	if ( n < 0 ) {
        perror( "lectura de socket" );
        exit( 1 );
        }
	printf("server: %s \n", buffer);
	return 0;
}
/**
	@brief startScanning

	a implementar

	@param a implementar
	@return a implementar
*/

int startScanning(int socket){
	printf("Ingresaste startScanning\n");
	char buffer[TAM];
	int n = read( socket, buffer, TAM );
    	if ( n < 0 ) {
        perror( "lectura de socket" );
        exit( 1 );
        }
	printf("server: %s \n", buffer);
	return 0;
}

/**
	@brief obtenerTelemetria

	a implementar

	@param a implementar
	@return a implementar
*/

int obtenerTelemetria(int socket){
	printf("Ingresaste  obtenerTelemetria\n");
	char buffer[TAM];
	int n = read( socket, buffer, TAM );
    	if ( n < 0 ) {
        perror( "lectura de socket" );
        exit( 1 );
        }
	printf("server: %s \n", buffer);
	return 0;
}
